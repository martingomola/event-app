export class Event {
    name: string;
    code: number;
    location: string;
    owner: any = { 
    };
    date_from: string;
    date_to: string;
}


export class GroupEvents {
     date_from : string;
     events: any[];
}