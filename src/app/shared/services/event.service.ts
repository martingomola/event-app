import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Event } from '../models/event';

@Injectable()
export class EventService {
    private eventUrl: string = './assets/data.json';
    
    constructor(private http: Http) {}

    /**
     * Load all events
     */
    getEvents(): Observable<Event[]> {
        return this.http.get(this.eventUrl)
            .map(res => res.json());
    }

    /**
     * Get one event
     */
    getEvent(id: number): Observable<Event> {
        return this.http.get(this.eventUrl)
        .map(res => res.json()
        .filter(<Event>(oneEvent) => oneEvent.event_id == id));
    }
    
    //user list
    getUserList(): Observable<Event> {
         return this.http.get(this.eventUrl)
        .map(res => res.json());
    }
    // create events

    // edit events

    // delete event

    // duplicate event

}