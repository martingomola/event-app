import { Injectable, Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';


@Pipe({
    name: 'searchPipe'
})

@Injectable()
export class SearchFilter implements PipeTransform {
    transform = function (items, term) {
        // console.log(items, term);
        if (term === undefined)
            return items;
        return items.filter(item => {
            if (item.name.toString().toLowerCase().includes(term.toLowerCase())) {
                //console.log('1');
                return true;
            }
            else if (item.location.toString().toLowerCase().includes(term.toLowerCase())) {
                // console.log('2');
                return true;
            }
            else if (item.owner.name.toString().toLowerCase().includes(term.toLowerCase())) {
                // console.log('3');
                return true;
            }
            // pre vsetky proprerties
            // for (let property in item) {
            //     if (item[property] === null) {
            //         continue;
            //     }
            //     if (item[property].toString().toLowerCase().includes(term.toLowerCase())) {
            //         return true;
            //     }
            // }
            // return false;
        })
    }
}