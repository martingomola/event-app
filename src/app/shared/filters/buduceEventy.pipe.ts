import { Injectable, Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';


@Pipe({
    name: 'BuduceEventy'
})

@Injectable()
export class BuduceEventy implements PipeTransform {
    transform(input, args?) {
        // let [min, max] = args;
        let todayDate = moment();
        let todayPlus7 = moment().add(7, 'days');
        //console.log('today', todayDate);
        return input.filter(event => {
            let eventStartFrom = moment(event.date_from);
            if (moment(eventStartFrom).isAfter(todayPlus7)) {
                return event
            }
            //return event.event_id >= +min && event.event_id<= +max;
        });
    }
}