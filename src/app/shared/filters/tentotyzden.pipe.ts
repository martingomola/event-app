import { Injectable, Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';


@Pipe({
    name: 'TentoTyzden'
})

@Injectable()
export class TentoTyzden implements PipeTransform {
    transform(input) {
        // let [min, max] = args;
        let todayDate = moment();
        let todayPlus7 = moment().add(7, 'days');
        // let daystoMonday = 0 - (todayDate.isoWeekday() - 1) + 7;
        // let nextMonday = todayDate.subtract(daystoMonday, 'days');
        //console.log('today', todayDate, 'do pondelka',daystoMonday, 'nextmonday', nextMonday );
        return input.filter(event => {
            let eventStartFrom = moment(event.date_from);
            if (moment(eventStartFrom).isBetween(todayDate, todayPlus7)) {
                return event
            }
            //return event.event_id >= +min && event.event_id<= +max;
        });
    }
}