import { Injectable, Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';


@Pipe({
    name: 'userFilter'
})

@Injectable()
export class UserFilter implements PipeTransform {
    transform = function (items, term) {
        // console.log(items, term);
        if (term === undefined)
            return items;
        return items.filter(item => {
            if (item.owner.name.toString().toLowerCase().includes(term.toLowerCase())) {
                // console.log('3');
                return true;
            }
        })
    }
}