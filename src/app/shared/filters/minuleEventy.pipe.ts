import { Injectable, Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';


@Pipe({
    name: 'MinuleEventy'
})

@Injectable()
export class MinuleEventy implements PipeTransform {
   transform(input, args?) {
    // let [min, max] = args;
    // let min:number = 3783;
    // let max:number = 3785;
    let todayDate: Date = new Date();
    let myMoment = moment(todayDate);   
   // console.log(myMoment);
    return input.filter(event => {
        let eventStartFrom =  moment(event.date_from);
        if  (moment(eventStartFrom).isBefore(todayDate)) {
            return event
        }
      //return event.event_id >= +min && event.event_id<= +max;
    });
  }
}