import { Injectable, Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';


@Pipe({
    name: 'orderBy'
})

@Injectable()
export class OrderBy implements PipeTransform {
    transform(value: any, args?: any): any {
        let newVal = value.sort((a: any, b: any) => {
            let date1 = new Date(a.date_from);
            let date2 = new Date(b.date_from);

            if (date1 > date2) {
                return 1;
            } else if (date1 < date2) {
                return -1;
            } else {
                return 0;
            }
        });

        return newVal;
    }
}