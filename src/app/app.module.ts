import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { routing } from './app.config';
import { MomentModule } from 'angular2-moment';

import { EventsComponent } from './components/events.component';
import { EventListComponent } from './components/eventList/eventList.component';
import { EventEditComponent } from './components/eventEdit/eventEdit.component';
import { EventAddComponent } from './components/eventAdd/eventAdd.component';
import { EventDetailComponent } from './components/eventDetail/eventDetail.component';
import { SearchBox } from './components/searchBox/searchBox.component';

import { MinuleEventy } from './shared/filters/minuleEventy.pipe';
import { TentoTyzden } from './shared/filters/tentotyzden.pipe';
import { BuduceEventy } from './shared/filters/buduceEventy.pipe';
import { SearchFilter } from './shared/filters/search.pipe';
import { OrderBy } from './shared/filters/orderBy.pipe';
import { UserFilter } from './shared/filters/userFilter.pipe';

import { AppComponent } from './app.component';
import { EventService } from './shared/services/event.service';
import { Ng2Bs3ModalModule } from 'ng2-bs3-modal/ng2-bs3-modal';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        routing,
        MomentModule,
        Ng2Bs3ModalModule,
    ],
    declarations: [
        AppComponent,
        EventsComponent,
        EventListComponent,
        EventEditComponent,
        EventAddComponent,
        EventDetailComponent,
        SearchBox,
        MinuleEventy,
        TentoTyzden,
        BuduceEventy,
        SearchFilter,
        OrderBy,
        UserFilter,
    ],
    providers:
    [
        EventService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }