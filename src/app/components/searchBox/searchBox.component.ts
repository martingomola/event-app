import { OnInit, Component, Output, EventEmitter } from "@angular/core";

@Component({
    selector: 'search-box',
    template: `
            <form class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <input #input type="text" class="form-control" placeholder="Search" (input)="update.emit(input.value)">
                </div>
            </form>`
}) 

export class SearchBox implements OnInit {
    @Output() update = new EventEmitter();

    ngOnInit(){
       this.update.emit(''); 
    }
}