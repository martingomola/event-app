import { Component, OnInit } from '@angular/core';

import { EventService } from '../../shared/services/event.service';
import { Event } from '../../shared/models/event';

@Component({
    selector: 'event-list',
    templateUrl: './eventList.component.html'
})

export class EventListComponent implements OnInit {
    events: Event[];
    searchTerm: string;
    userList: string[] = [];
    userName: string = '';


    constructor(private service: EventService) {

    }

    activeEvent: Event;
    //search term
    searchUpdate(evt) {
        this.searchTerm = evt;
        console.log(this.searchTerm);
    }

    // edit event
    editEvent(event) {
        this.activeEvent = event;
        console.log('kliknuty event', this.activeEvent);
    }
    //delete event 
    deleteEvent(event) {
        console.log('vyfiltruj event', event);
        this.events = this.events.filter(item => item !== event);
    }

    // add new event
    onEventCreated(event) {
        // console.log('onEventCreated', event.event);
        this.events.push(event.event);
        //refres array
        this.events = this.events.slice();
        console.log('added', this.events);
      
    }

    // clone event
    cloneObj(o) {
        return JSON.parse(JSON.stringify(o));
    }

    cloneEvent(event) {
        const clonedObj: any = {} = this.cloneObj(event);
        this.events.push(clonedObj);
        //refres array
        this.events = this.events.slice();
        console.log('clone test', this.events);
    }

    // filter uzivatelov
    filterUsers(allEvents) {
        let keys = [];
        this.events.forEach(function (item) {
            let key = item.owner.name;
            if (keys.indexOf(key) === -1) {
                keys.push(key);
                this.userList.push(item.owner.name);
            }
        }, this);
    }

    // selectUser

    selectUser(user){
        this.userName = user;
    }

    ngOnInit() {
        //load all events
        this.service.getEvents()
            .subscribe(events => {
                this.events = events;
                // console.log('all events',this.events);
                this.filterUsers(this.events);
            });
        // console.log('te1111t', this.update);
    }
}