import { Component, Output, EventEmitter } from '@angular/core';
import { Event } from '../../shared/models/event';

@Component({
	selector: 'event-add',
	templateUrl: './eventAdd.component.html'
})

export class EventAddComponent {
	@Output() eventCreated = new EventEmitter();
	newEvent: Event = new Event();
	active: boolean = true;

	onSubmit() {
		//default data
		this.newEvent.owner.name = 'Owner Martin';
		this.newEvent.code = 1212;
		this.newEvent.location = 'Bratislava, Slovak Republic';
		this.newEvent.date_from ='2017-02-22T00:00:00';
    	this.newEvent.date_to = '2017-02-28T00:00:00+01:00';

		this.eventCreated.emit({ event: this.newEvent });
		
		//clear form
		this.newEvent = new Event();
		console.log('submit ide', this.newEvent);
		this.active = false;
		setTimeout(() => this.active = true, 0 );
	}
} 