import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Event } from '../../shared/models/event';
import { EventService } from '../../shared/services/event.service';

@Component({
    selector: 'event-detail',
    templateUrl: './eventDetail.component.html'
})

export class EventDetailComponent implements OnInit {
    event: Event;

    constructor(
        private route: ActivatedRoute,
        private service: EventService){}

    ngOnInit() {
        let id = this.route.snapshot.params['id'];
        console.log(id);

        //this events.find(event => event.event_id === id)

        this.service.getEvent(id)
        .subscribe(event => this.event = event);

    }

}