import {Component, Input, OnInit } from '@angular/core';

@Component({
	selector: 'event-edit',
	templateUrl: './eventEdit.component.html'
})

export class EventEditComponent implements OnInit  {
	@Input() event: Event;

	constructor(){
		
	}
	
	ngOnInit() {
		//Called after the constructor, initializing input properties, and the first call to ngOnChanges.
		//Add 'implements OnInit' to the class.
		
	}

	/**
	 * Edit Event
	 */
	updateEvent(){
		console.log('update event');
		console.log(this.event);
	}
}