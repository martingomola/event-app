import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http } from '@angular/http';

import { Event } from './shared/models/event';
import { EventService } from './shared/services/event.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    constructor(
        private service: EventService,
        private router: Router
        ) {}

    ngOnInit() {
    }

}