import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EventsComponent } from './components/events.component';
import { EventListComponent } from './components/eventList/eventList.component';
import { EventAddComponent } from './components/eventAdd/eventAdd.component';
import { EventDetailComponent } from './components/eventDetail/eventDetail.component';



export const routes: Routes = [
  {
    path: '',
    redirectTo: '/events',
    pathMatch: 'full'
  },
  {
    path: 'events',
    component: EventsComponent,
    children: [
      {
        path: '',
        component: EventListComponent
      },
      {
        path: ':id',
        component: EventDetailComponent
      }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);